.. testsphinx documentation master file, created by
   sphinx-quickstart on Thu Apr 30 09:35:21 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to testsphinx's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   example1
   example2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
